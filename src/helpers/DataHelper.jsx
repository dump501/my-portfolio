import Icon from "../Components/Icon/Icon"
import IconJava from "../Components/Icon/IconJava"
import IconJavascript from "../Components/Icon/IconJavascript"
import IconTypescript from "../Components/Icon/IconTypescript"


import service64 from "../images/projects/service64.png";
import usocialbot from "../images/projects/usocialbot.png";
import usocialfamily from "../images/projects/usocialfamily.png";

const projects = [
  {
    title: "Service 64",
    image: service64,
    demo: "https://service64.com/",
    description: "A web application for finding local services providers in Bangladesh",
    tools: [
      {
        name: "React js",
        role: "used for frontend"
      },
      {
        name: "Mongodb",
        role: "used as DBMS"
      },
      {
        name: "Node js",
        role: "used for backend"
      },
      {
        name: "Git",
        role: "Used as versionning control System"
      },
      {
        name: "GitLab",
        role: "Used to host our repository"
      },
    ],
    challenges: "My main work here was to make the search user friendly and debug the approval / desapproval of workers on the admin panel"
  },
  {
    title: "Usocial family",
    image: usocialfamily,
    demo: "https://usocialfamily.com/",
    description: "A web application for finding local services providers in Cameroon",
    tools: [
      {
        name: "Symfony",
        role: "Used to build"
      },
      {
        name: "Git",
        role: "Used as versionning control System"
      },
      {
        name: "GitLab",
        role: "Used to host our repository"
      },
    ],
    challenges: "The main challenge here was to make the website like a CMS such that the admin can add content easily"
  },
  {
    title: "Usocial family",
    image: usocialfamily,
    demo: "https://usocialfamily.com/",
    description: "A web application for finding local services providers in Cameroon",
    tools: [
      {
        name: "Symfony",
        role: "Used to build"
      },
      {
        name: "Git",
        role: "Used as versionning control System"
      },
      {
        name: "GitLab",
        role: "Used to host our repository"
      },
    ],
    challenges: "The main challenge here was to make the website like a CMS such that the admin can add content easily"
  },
  {
    title: "Usocialbot",
    image: usocialbot,
    demo: "https://usocialbot.com/",
    description: "A web application to present Usocialbot app",
    tools: [
      {
        name: "Symfony",
        role: "Used to build the web application"
      },
      {
        name: "MYSQL",
        role: "Used as DBMS"
      },
      {
        name: "Git",
        role: "Used as versionning control System"
      },
      {
        name: "GitLab",
        role: "Used to host our repository"
      },
    ],
    challenges: "The main challenge here was to make the website like a CMS such that the admin can add content easily"
  },
]

const technoSkills = [
  {
    title: "Javascript",
    percentage: "90%",
    icon: <IconJavascript height={50} width={50} />
  },
  {
    title: "Typescript",
    percentage: "80%",
    icon: <IconTypescript height={50} width={50} />
  },
  {
    title: "Java",
    percentage: "70%",
    icon: <IconJava height={50} width={50} />
  },
  {
    title: "Node JS",
    percentage: "85%",
    icon: <Icon name="nodejs" height={50} width={50} />
  },
]

export {
  projects,
  technoSkills
}