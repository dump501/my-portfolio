import { useState } from 'react';
import ProjectCard from './Components/ProjectCard/ProjectCard';
import ContactForm from './Components/ContactForm/ContactForm';
import Typer from './Components/Typer/Typer';

import profile from "./Components/Images/profile.png";
import about from "./Components/Images/about.png";

import RadarChart from './Components/RadarChart/RadarChart';
import TechnoSkill from './Components/TechnoSkill';
import Popup from './Components/Popup/Popup';
import { projects, technoSkills } from './helpers/DataHelper';





function App() {
  const [visible, setvisible] = useState(false)
  const [currentProject, setcurrentProject] = useState(projects[0])

  const showPopup = (project) => {
    setcurrentProject(project)
    setvisible(true)
  }

  return (
    <div className="wrapper">
      <div className='container'>
        <section id="home" className='home'>
          <img className='home_profile' src={profile} />
          <div className='home_left'>
            <div className='home_title'>
              Hi, I am Fritz
            </div>
            <div className='home_subtitle'>
              <Typer />
            </div>
            <div className='home_content'>
              I am a fullstack developer with blockchain experience.
            </div>
            {/* <a href="#" className='home_button'>Resume</a> */}
          </div>
        </section>

        <section id="about" className='about'>
          <div className='section_title'>About me</div>
          <div className='about_section'>
            <div className='about_left'>
              <div className='section_subtitle'>A bit about me</div>
              <div className='about_content'>
                I'm a fullstack Developer with experience in designing new features from
                ideation to production, implementation of wireframes into high performance software applications. I passionately combine good
                design, technology, and innovation in all my projects, which I like to accompany from the first idea to release.
              </div>
            </div>
            <div className='about_content_image'>
            <img src={about} />
            </div>
          </div>

          <div style={{ height: 25, width: 25 }}>
          </div>
        </section>

        <section className='skills'>
          <div className='section_title'>Skills</div>
          <div className='section_subtitle'>Why me as developer</div>
          <div>
            Using a combination of cutting-edge technologies and reliable open-source software I build user-focused, performant apps and websites
            for smartphones, tablets, and desktops.
          </div>
          {/* <div className='section_subtitle'>Frameworks and tools I use</div> */}
          <div className='skills_content'>
            <div className='skills_languages'>
              {/* <div className='section_subtitle'>Programming languages</div> */}
              {technoSkills.map((item, i) => <TechnoSkill key={i} title={item.title} percentage={item.percentage} icon={item.icon} />)}
            </div>
            <div className='skills_frameworks'>
              <RadarChart />
            </div>
          </div>

        </section>

        <section id="projects" className='projects'>
          <div className='section_title'>Projects</div>
          <div className='section_subtitle'>What I build</div>
          <div className='projects_cards'>
            {projects && projects.map((project, i) => <ProjectCard
              key={i}
              setcurrentProject={showPopup}
              project={project}
            />)}
          </div>
        </section>

        <section id="contact" className='contact'>
          <div className='section_title'>Contact</div>
          <div className='contact_infos'>
            <div>
              <div className='section_subtitle'>Connect with me</div>
              <div style={{ marginBottom: "1rem" }} className='about_content'>
                If you want to know more about me or my work, or if you would just
                like to say hello, send me a message. I'd love to hear from you.
              </div>
              <ContactForm />
            </div>
            <div className='infos'>
              {/* <p className='infos_label'>Email</p> */}
              <p className='infos_content'>tsafack07albin@gmail.com</p>
              {/* <p className='infos_label'>Phone</p> */}
              <p className='infos_content'>+237698406818</p>
              {/* <p className='infos_label'>Address</p> */}
              <p className='infos_content'>Dschang, Cameroon</p>
            </div>
          </div>
        </section>

        <footer className='footer'>
          Made with ❤️ by Tsafack Fritz
        </footer>
        {currentProject && <Popup visible={visible} setvisible={setvisible}>
          <img src={currentProject.image} style={{width: "100%", height: "auto"}}/>
          <br />
          <h4>{currentProject.title}</h4>
          {currentProject.description}
          <br />
          <h4>Challenges</h4>
          {currentProject.challenges}
          <h4>Tools used</h4>
          {currentProject.tools.map((tool, i) => <span key={i}>{tool.name} {i+1 == currentProject.tools.length ? "." : " | "} </span>)}
          <br /><br />
          <button className='btn' onClick={(e) => { setvisible(false) }}>close</button>&nbsp;
          <button className='btn' onClick={(e) => { setvisible(false) }}>close</button>
        </Popup>}
      </div>
    </div>
  );
}

export default App;
