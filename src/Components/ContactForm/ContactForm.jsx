import React, {useRef} from 'react'
import emailjs from '@emailjs/browser'
import "./ContactForm.css"

function ContactForm() {

    const form = useRef()

    const sendEmail = (e)=>{
        e.preventDefault();

        emailjs.sendForm('contact_service', 'contact_form', form.current, '7zVC8ho7XH5HOL9T1')
        .then((result) =>{
            console.log(result.text);
            alert("Thanks for contacting me I'll give you a feed back ASAP");
            form.current.reset()
        }, (error)=>{
            console.log(error.text);
            alert("OOPs! an error occured please try later. sorry for the desagrement");
        })
    }


  return (
    <form ref={form} onSubmit={sendEmail} className='contact_form'>
        <div className='contact_form_field'>
            <label className='contact_form_label'>Name</label>
            <input name="user_name" className='input' type="text" placeholder="Enter your name" />
        </div>
        <div className='contact_form_field'>
            <label className='contact_form_label'>Email</label>
            <input name="user_email" className='input' type="email" placeholder="Enter your email" />
        </div>
        <div className='contact_form_field'>
            <label className='contact_form_label'>Message</label>
            <textarea name="message" className='input textarea' placeholder="Enter your message"></textarea>
        </div>
        <div className='contact_form_actions'>
            <a href="mailto:tsafack07albin@gmail.com">Send me email directly</a>
            
            <button type='submit' className='contact_form_submit'>Submit</button>
        </div>
    </form>
  )
}

export default ContactForm