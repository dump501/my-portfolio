import React from 'react'
import {
  Chart as ChartJS,
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend,
} from 'chart.js';
import { Radar } from 'react-chartjs-2';

ChartJS.register(
  RadialLinearScale,
  PointElement,
  LineElement,
  Filler,
  Tooltip,
  Legend
);

const data = {
  labels: ['Javascript', 'Java', 'PHP', 'Typescript',
    'Nodejs', 'Golang'],
  datasets: [
    {
      label: 'Frameworks and tools',
      data: [9, 9, 8, 9, 9, 8],
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'white',
      borderWidth: 5,
      fill: true,
      color: "white"
    },
  ],
};

const RadarChart = () => {
  return (
    <div style={{ width: "100%" }}>
      <Radar
        options={{
          responsive: true,
          scales: {
            r: {
              min: 0,
              angleLines: {
                color: 'rgba(156, 0, 224, 1)'
              },
              grid: {
                color: 'grey',
                lineWidth: 1,
              },
              pointLabels: {
                color: 'white',
                font: {
                  size: "20rem"
                }
              },
              ticks: {
                color: 'red',
                // stepSize: .5
              }
            }
          },
          elements: {
            line: {
              fill: true
            }
          },
          plugins: {
            legend: {
              labels: {
                color: "white"
              },
              position: "bottom"
            }
          }
        }}
        data={data} />
    </div>
  )
}

export default RadarChart
