import React from 'react'

const Popup = ({children, visible, setvisible}) => {
    if(visible){
        return(
            <div className='popup-container'>
                <div className='box'>
                    <button onClick={e => setvisible(false)} className=" btn close">X</button>
                    {children}
                </div>
            </div>
        )
    }
    return <></>
}

export default Popup
