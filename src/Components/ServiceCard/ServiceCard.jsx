import React from 'react'
import Icon from '../Icon/Icon'
import "./ServiceCard.css"

function ServiceCard({title,  image}) {
  return (
    <div className='service_card'>
        <div className='service_card_icon'>
            <img height={100} src={image} />
        </div>
        <div className='service_card_title'>{title}</div>
        {/* <div className='service_card_content'>{content}</div> */}
    </div>
  )
}

export default ServiceCard