import React from 'react'
import "./ProjectCard.css"
import image from "../Images/image.png"

function ProjectCard({project, setcurrentProject}) {
  return (
    <div className='project_card'>
        <img className='project_card_image' src={project.image} />
        <div className='project_card_details'>
            <div className='project_card_title'>{project.title}</div>
            <div className='project_card_content'>{project.description}</div>
            <div className='project_card_links'>
                <button className='project_card_link' onClick={()=>setcurrentProject(project)}>Details</button>
            </div>
        </div>
    </div>
  )
}

export default ProjectCard