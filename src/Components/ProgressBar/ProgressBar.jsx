import React from 'react'

const ProgressBar = ({percentage, color}) => {
  return (
    <div className='progress-bar'>
      <span style={{width: percentage? percentage: "50%", backgroundColor: color ? color: "rgb(43, 194, 83)"}}></span>
    </div>
  )
}

export default ProgressBar
