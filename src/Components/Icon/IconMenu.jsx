import React from 'react'

function IconMenu({height, width, color}) {
  return (
<svg fill={color?color:"#000"} height={height?height:25} width={width?width:25} viewBox="0 0 32 32" enable-background="new 0 0 32 32">
    <path d="M26,16c0,1.104-0.896,2-2,2H8c-1.104,0-2-0.896-2-2s0.896-2,2-2h16C25.104,14,26,14.896,26,16z" id="XMLID_314_"/>
    <path d="M26,8c0,1.104-0.896,2-2,2H8c-1.104,0-2-0.896-2-2s0.896-2,2-2h16C25.104,6,26,6.896,26,8z" id="XMLID_315_"/>
    <path d="M26,24c0,1.104-0.896,2-2,2H8c-1.104,0-2-0.896-2-2s0.896-2,2-2h16C25.104,22,26,22.896,26,24z" id="XMLID_316_"/>
</svg>

  )
}

export default IconMenu