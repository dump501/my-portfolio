import React from 'react'
import icons from './icons'

function Icon({name, width, height, color}) {
  return (
    <svg viewBox={icons[name].viewBox} height={height?height:25} width={width?width:25}>
        <path fill={color?color:icons[name].fill} d={icons[name].d}></path>
    </svg>
  
  )
}

export default Icon