import React from 'react'
import Icon from '../Icon/Icon'
import "./TechnoItem.css"

function TechnoItem({icon, name}) {
  return (
    <div className='techno_item'>
        {icon}
        <div className='techno_item_name'>{name}</div>
    </div>
  )
}

export default TechnoItem