import React, { useEffect, useState } from 'react'
import IconMenu from '../Icon/IconMenu'
import "./Navbar.css"
function Navbar({wrapper}) {
    const [isOpen, setisOpen] = useState(false)
    const [activeNav, setactiveNav] = useState("#home")


    const navs = [
      {id: "home", name:"Home"},
      {id: "about", name:"About"},
      {id: "services", name:"Services"},
      {id: "projects", name:"Projects"},
      {id: "contact", name:"Contact"},
  ]


    const toggleNav = ()=>{
        setisOpen(!isOpen)
    }

    const handleClick = (e, id)=>{
        // console.log(e.target, i, e.target === e.currentTarget);
        // check if it is the parent that is clicked
        if(e.target !== e.currentTarget){
            let top = window.scrollY;
            let offset = document.querySelector(activeNav).offsetTop;
            let height = document.querySelector(activeNav).offsetHeight;
            if (top >= offset && top < offset + height) {
                handleNavChange(`${e.target.parentElement.getAttribute("href")}`)
            }
        //  document.querySelector(`${e.target.parentElement.getAttribute("href")}`).scrollIntoView({behavior: "smooth"});
        //  setactiveNav("#"+id)
        }
    }

    const handleNavChange = (id)=>{
        document.querySelector(`${id}`).scrollIntoView({behavior: "smooth"});
        setactiveNav(id)
    }

    const handleScroll = ()=>{
        console.log("scrolled");
        let section = document.querySelectorAll('section')
        // section.forEach(sec => {
            let top = window.scrollY;
            let offset = document.querySelector(activeNav).offsetTop;
            let height = document.querySelector(activeNav).offsetHeight;
            let id = document.querySelector(activeNav).getAttribute('id');

            if (top >= offset && top < offset + height) {
                const target = document.querySelector(`[href='#${id}']`);
                console.log(activeNav);
                handleNavChange("#"+id);
                // setactiveNav("#"+id)
                console.log(activeNav);
            }
        // })
    }

    useEffect(()=>{
        wrapper.current.addEventListener("scroll", handleScroll)
        return ()=>{
            wrapper.current.removeEventListener("scroll", handleScroll)
        }
    }, [])

    

  return (
    <div className='navbar'>
        <div className='navbar_brand'>Tsafack Fritz</div>
        <div className='navbar_items'>
            {
                navs.map((nav, i)=><a onClick={(e)=>{e.preventDefault(); handleClick(e, nav.id)}} key={i} href={`#${nav.id}`} className={`${("#"+nav.id)===activeNav?"active":""}`}>
                    <div className='navbar_item'>{nav.name}</div>
                </a>)
            }
            
        </div>
        <div className='navbar_toggle'>
            <span onClick={(e)=>{e.preventDefault(); toggleNav()}}><IconMenu color="white" height={30} width={30} /></span>
            {isOpen && <div className='mobile_navbar_items'>
                <div>
                    <div className='mobile_navbar_item'>Home</div>
                    <div className='mobile_navbar_item'>About</div>
                    <div className='mobile_navbar_item'>Services</div>
                    <div className='mobile_navbar_item'>Projects</div>
                    <div className='mobile_navbar_item'>Contact</div>
                </div>
            </div>}
        </div>
    </div>
  )
}

export default Navbar