import React from 'react'
import ProgressBar from './ProgressBar/ProgressBar'

const TechnoSkill = ({title, percentage, icon}) => {
  return (
    <div className='techno'>
      <div className="techno_title"> {icon} &nbsp; {title}</div>
      <ProgressBar color="white" percentage={percentage} />
    </div>
  )
}

export default TechnoSkill
