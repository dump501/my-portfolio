//App.test.tsx
import { describe, it } from 'vitest';
import { render, screen } from '../TestHelper';
import ContactForm from '../../Components/ContactForm/ContactForm';
describe('Given the ContactForm component', () => {
    // describe -> Used to group the test and used to describe what is currently being tested
    it('should display form inputs', () => {
        // it or test -> Individual test which is run by Vitest. It can either pass or fail
        render(<ContactForm />);
        expect(screen.getByText(/Name/i)).toBeInTheDocument();
        // expect -> is used to create assertions. In this context assertions are functions that can be called to assert a statement.
    });
    // it('should increment the count when icon (+) clicked', async () => {
    //     render(<App />);
    //     userEvent.click(screen.getByText('+'));
    //     expect(await screen.findByText(/count is: 1/i)).toBeInTheDocument();
    // });
    // it('should decrement the count when icon (-) clicked', async () => {
    //     render(<App />);
    //     userEvent.click(screen.getByText('-'));
    //     expect(await screen.findByText(/count is: -1/i)).toBeInTheDocument();
    // });
});