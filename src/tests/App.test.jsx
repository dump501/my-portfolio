//App.test.tsx
import { describe, it } from 'vitest';
import App from '../App';
import { render, screen } from './TestHelper';
describe('My portfolio', () => {
    // describe -> Used to group the test and used to describe what is currently being tested
    it('should display home section', () => {
        // it or test -> Individual test which is run by Vitest. It can either pass or fail
        render(<App />);
        expect(screen.getByText(/Hi, I am Fritz/i)).toBeInTheDocument();
        // expect -> is used to create assertions. In this context assertions are functions that can be called to assert a statement.
    });
    it('should display about section', () => {
        // it or test -> Individual test which is run by Vitest. It can either pass or fail
        render(<App />);
        expect(screen.getByText(/A bit about me/i)).toBeInTheDocument();
        // expect -> is used to create assertions. In this context assertions are functions that can be called to assert a statement.
    });
    
    it('should display skills section', () => {
        // it or test -> Individual test which is run by Vitest. It can either pass or fail
        render(<App />);
        expect(screen.getByText(/Why me as developer/i)).toBeInTheDocument();
        // expect -> is used to create assertions. In this context assertions are functions that can be called to assert a statement.
    });
    // it('should increment the count when icon (+) clicked', async () => {
    //     render(<App />);
    //     userEvent.click(screen.getByText('+'));
    //     expect(await screen.findByText(/count is: 1/i)).toBeInTheDocument();
    // });
    // it('should decrement the count when icon (-) clicked', async () => {
    //     render(<App />);
    //     userEvent.click(screen.getByText('-'));
    //     expect(await screen.findByText(/count is: -1/i)).toBeInTheDocument();
    // });
});